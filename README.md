# jekyll-unique-urls

When you have several pages with the same URL, Jekyll overwrites their
contents.  This plugin prevents URL multiplication by adding a unique
hash to duplicate URLs.

## Installation

Add this line to your site's Gemfile:

```ruby
gem 'jekyll-unique-urls'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install jekyll-unique-urls

## Usage

Add the plugin to your `_config.yml`:

```yaml
plugins:
- jekyll-unique-urls
```

## Contributing

Bug reports and pull requests are welcome on 0xacab.org at
<https://0xacab.org/sutty/jekyll/jekyll-unique-urls>. This project is
intended to be a safe, welcoming space for collaboration, and
contributors are expected to adhere to the [Sutty code of
conduct](https://sutty.nl/en/code-of-conduct/).

If you like our plugins, [please consider
donating](https://donaciones.sutty.nl/en/)!

## License

The gem is available as free software under the terms of the GPL3
License.

## Code of Conduct

Everyone interacting in the jekyll-unique-urls project’s codebases,
issue trackers, chat rooms and mailing lists is expected to follow the
[code of conduct](https://sutty.nl/en/code-of-conduct/).
