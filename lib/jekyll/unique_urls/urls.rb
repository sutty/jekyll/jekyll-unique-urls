# frozen_string_literal: true

module Jekyll
  module UniqueURL
    # Adds a method #urls to keep a unique set of URLs from which to
    # check if the current URL is unique.
    module URLs
      def self.included(base)
        base.class_eval do

          # All URLs from the site.
          #
          # @return [Set]
          def urls
            @urls ||= Set.new
          end

          # Also reset URLs between resets, to make it compatible with
          # jekyll-locales
          alias reset_all reset
          def reset
            @urls = nil
            reset_all
          end
        end
      end
    end
  end
end
