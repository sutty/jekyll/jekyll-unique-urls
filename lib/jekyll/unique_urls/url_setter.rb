# frozen_string_literal: true

module Jekyll
  module UniqueURL
    # Modifies the #url method to check if the URL is a duplicate
    module URLSetter
      def self.included(base)
        base.class_eval do
          alias original_url url

          # Only do this once.  The first time we check if the URL
          # already exists and add a unique string to it.
          def url
            return @url if @url

            original_url

            if @site.urls.include? @url
              @url = unique_url

              Jekyll.logger.warn "URL for #{relative_path} is duplicate, changing to #{@url}"
            end

            @site.urls << @url

            @url
          end

          # Make the URL unique by appending a unique string to it.  If
          # it's a directory, add it to the directory; if it's a file,
          # add it to the file name.
          def unique_url
            if @url.end_with? '/'
              @url.sub(%r{/\z}, '-' + digest + '/')
            elsif respond_to? :output_ext
              @url.sub(%r{#{output_ext}\z}, '-' + digest + output_ext)
            else
              @url.sub(%r{#{extname}\z}, digest + extname)
            end
          end

          # Paths are unique for every resource type so we just digest
          # those and take the first 8 chars.
          #
          # XXX: What's the chance of collisions?
          def digest
            @digest ||= Digest::SHA256.hexdigest(relative_path)[0..7]
          end
        end
      end
    end
  end
end
