# frozen_string_literal: true

require_relative 'unique_urls/urls'
require_relative 'unique_urls/url_setter'

Jekyll::Site.include Jekyll::UniqueURL::URLs
Jekyll::Page.include Jekyll::UniqueURL::URLSetter
Jekyll::Document.include Jekyll::UniqueURL::URLSetter
